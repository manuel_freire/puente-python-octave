from flask import Flask, escape, request
from oct2py import octave
from re import sub

app = Flask(__name__)
    
@app.route('/x')
def hello():
    script = request.args.get("a", "t.m")
    arg1 = int(request.args.get("b", "10"))
    arg2 = int(request.args.get("c", "10"))
    arg3 = int(request.args.get("d", "10"))
    result = octave.feval(script, arg1, arg2, arg3)
    return sub(r'[\[\].]*', '', f'{escape(result)}')

