Necesitas python y oct2py instalados, así como un `octave` disponible.

Para lanzar el servidor:

    FLASK_APP="test.py" flask run

Para acceder al servidor, en su configuración por defecto:

    http://127.0.0.1:5000/x?a=t.m&b=100&c=20
    
    (llama al script `t.m` con argumentos `100` y `20`
    
    

